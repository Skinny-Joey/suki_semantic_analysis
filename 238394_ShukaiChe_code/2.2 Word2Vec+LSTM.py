import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn import metrics
from keras.callbacks import EarlyStopping
from keras.layers import Dropout
from keras.models import load_model
from gensim.models import KeyedVectors
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from tqdm import tqdm
from nltk import pos_tag
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer

data = pd.read_csv('fin_data_run.csv', encoding='unicode_escape')
data = data.sample(frac=1).reset_index(drop=True)

# The part of speech
def get_wordnet_pos(tag):
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return None

wnl = WordNetLemmatizer()
# nltk.download('stopwords')
stop = stopwords.words('english')
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')

for i in tqdm(range(len(data))):
    sentence = data.iloc[i, 0]
    words = word_tokenize(sentence)
    tagged_sent = pos_tag(words)
    new_words = []
    for j in range(len(words)):
        if words[j].isdigit():
            new_words.append('NUM')  # num normalization
        elif words[j] in stop or not words[j].isalpha():  # remove stopwords
            continue
        else:
            wordnet_pos = get_wordnet_pos(tagged_sent[j][1]) or wordnet.NOUN
            temp = wnl.lemmatize(tagged_sent[j][0], pos=wordnet_pos)  # lemmatization
            new_words.append(temp.lower())  # case normalization
    new_sentence = ''
    for w in new_words:
        new_sentence += w + ' '
    data.iloc[i, 0] = new_sentence
text = data['text']
label = data['label']

# Using tokenizer, each word in the text is represented by a number, which is a preprocessing of word vectorization
# Set the 50000 most frequently used words
# (texts_to_matrix will take the pre-max_nb_words column, will take the pre-max_nb_words column)
# the occurrence of too low frequency words is not considered
MAX_NB_WORDS = 50000
MAX_SEQUENCE_LENGTH = 250
EMBEDDING_DIM = 300

tokenizer = Tokenizer(num_words=MAX_NB_WORDS, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
tokenizer.fit_on_texts(data['text'].values)
word_index = tokenizer.word_index
print('%s different words.' % len(word_index))

w2v_file_path = 'GoogleNews-vectors-negative300.bin'  # w2v from google
w2v_model = KeyedVectors.load_word2vec_format(w2v_file_path, binary=True)
vocab_list = list(tokenizer.word_index.keys())
# vocab_list = [word for word, Vocab in w2v_model.key_to_index.items()]
word_index = tokenizer.word_index
# word_index = {"":0}
word_vector = {}
embeddings_matrix = np.zeros((len(vocab_list) + 1, w2v_model.vector_size))

for i in tqdm(range(len(vocab_list))):
    word = vocab_list[i]  # Each word
    # word_index[word] = i + 1 #
    if word in w2v_model:
        word_vector[word] = w2v_model[word]
        embeddings_matrix[i + 1] = word_vector[word]  # Word vector matrix
        continue
    word_vector[word] = word_vector[list(word_vector.keys())[-1]]  # Words: word vectors
    embeddings_matrix[i + 1] = word_vector[word]  # Word vector matrix

embedding_layer = Embedding(input_dim=len(embeddings_matrix),  # The length of the dictionary
                            output_dim=EMBEDDING_DIM,  # Word vector length (100)
                            weights=[embeddings_matrix],  # Key points: pre-trained word vector coefficients
                            input_length=MAX_SEQUENCE_LENGTH,  # Maximum length of each sentence
                            trainable=False  # Whether to update the word vector during training
                            )

X = tokenizer.texts_to_sequences(data['text'].values)
X = pad_sequences(X, maxlen=MAX_SEQUENCE_LENGTH)
Y = pd.get_dummies(data['label']).values

# cross-validation
results = []
k = 10  # 10-fold cross-validation
cont_times = 0
for train_index, test_index in tqdm(KFold(k).split(X, Y)):
    cont_times += 1
    # model
    model = Sequential()
    model.add(embedding_layer)
    model.add(SpatialDropout1D(0.2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(3, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())

    epochs = 5
    batch_size = 64
    history = model.fit(X[train_index], Y[train_index], epochs=epochs, batch_size=batch_size, validation_split=0.1,
                        callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)])

    # model = load_model('lstm_class.h5')
    result = {}
    y_pred = model.predict(X[test_index])
    y_pred = y_pred.argmax(axis=1)
    y_test = Y[test_index].argmax(axis=1)
    result['accuracy'] = metrics.accuracy_score(y_test, y_pred)
    result['f1-macro'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='macro')
    result['f1-micro'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='micro')
    result['f1-weighted'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='weighted')
    result['precision-macro'] = metrics.precision_score(y_test, y_pred, average='macro')
    result['precision-micro'] = metrics.precision_score(y_test, y_pred, average='micro')
    result['precision-weighted'] = metrics.precision_score(y_test, y_pred, average='weighted')
    result['recall-macro'] = metrics.recall_score(y_test, y_pred, average='macro')
    result['recall-micro'] = metrics.recall_score(y_test, y_pred, average='micro')
    result['recall-weighted'] = metrics.recall_score(y_test, y_pred, average='weighted')
    results.append(result)

print('\n\nAll results')
df = pd.DataFrame(results)
print(df)
mean_df = df.mean()
mean_df = mean_df.to_dict()
print('Cross-validation results (average of all results)：')
print(mean_df)
results.append(mean_df)
df = pd.DataFrame(results)
print("The cross-validation result is placed in the last line of the result")
print(df)
df.to_csv('lstm_cross_validation_result_fin.csv')
print("Results saveo in lstm_cross_validation_result_fin.csv")
