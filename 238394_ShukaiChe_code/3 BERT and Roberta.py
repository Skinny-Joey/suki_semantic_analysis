import pandas as pd
from simpletransformers.classification import ClassificationModel
from sklearn.metrics import f1_score, accuracy_score
from tqdm import tqdm
from sklearn.model_selection import KFold
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn import metrics

data = pd.read_csv('tripadvisor_hotel_reviews.csv', encoding='unicode_escape')
data = data.sample(frac=1).reset_index(drop=True)
for i in range(len(data)):
    data.iloc[i, 1] = data.iloc[i, 1] - 1

k = 10  # 10-fold cross-validation
i = 0
results = []
for train_index, test_index in KFold(k).split(data):
    # Data
    train_df = data.iloc[train_index]
    eval_df = data.iloc[test_index]
    train_df.columns = ['text', 'labels']
    eval_df.columns = ['text', 'labels']
    # Creating a classification model(BERT,RoBERTa)
    # BERT
    model = ClassificationModel('bert', 'bert-base-uncased', num_labels=5, args={"output_dir": "./tmp/outputs_bert"+str(i)+"/",
                                                                               # "reprocess_input_data": True
                                                                               })
    # RoBERTa
    model = ClassificationModel('roberta', 'roberta-base', num_labels=5, args={"output_dir": "./tmp/outputs_roberta"+str(i)+"/",
                                                                               # "reprocess_input_data": True
                                                                               })

    # Train model
    model.train_model(train_df)

    # Evaluation model
    def f1_multiclass(labels, preds):
        return f1_score(labels, preds, average='micro')

    # result, model_outputs, wrong_predictions = model.eval_model(eval_df)
    result, model_outputs, wrong_predictions = model.eval_model(eval_df, f1=f1_multiclass, acc=accuracy_score)
    print('result', result)
    print('model_outputs', model_outputs)
    model_outputs = model_outputs.argmax(axis=1)
    print('model_outputs', model_outputs)
    print('wrong_predictions', wrong_predictions)

    result = {}
    y_pred = model_outputs
    y_test = eval_df['labels'].values
    result['accuracy'] = metrics.accuracy_score(y_test, y_pred)
    result['f1-macro'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='macro')
    result['f1-micro'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='micro')
    result['f1-weighted'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='weighted')
    result['precision-macro'] = metrics.precision_score(y_test, y_pred, average='macro')
    result['precision-micro'] = metrics.precision_score(y_test, y_pred, average='micro')
    result['precision-weighted'] = metrics.precision_score(y_test, y_pred, average='weighted')
    result['recall-macro'] = metrics.recall_score(y_test, y_pred, average='macro')
    result['recall-micro'] = metrics.recall_score(y_test, y_pred, average='micro')
    result['recall-weighted'] = metrics.recall_score(y_test, y_pred, average='weighted')
    results.append(result)
    i += 1

print('\n\n所有结果：')
df = pd.DataFrame(results)
print(df)
mean_df = df.mean()
mean_df = mean_df.to_dict()
print('交叉验证结果（所有结果的平均值）：')
print(mean_df)
results.append(mean_df)
df = pd.DataFrame(results)
print("交叉验证结果已放入结果的最后一行")
print(df)
df.to_csv('bert_cross_validation_hotel.csv')
print("结果已保存到bert_cross_validation_hotel.csv")
