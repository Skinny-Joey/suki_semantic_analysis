from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from tqdm import tqdm
from nltk import pos_tag
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
from gensim.models import KeyedVectors
from gensim.models.word2vec import Word2Vec
from sklearn.model_selection import KFold
from scipy.stats import multivariate_normal
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn import metrics
import numpy as np
import pandas as pd
from sklearn.naive_bayes import GaussianNB

data = pd.read_csv('tripadvisor_hotel_reviews.csv')
# Get word parts
def get_wordnet_pos(tag):
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return None

# Load model
wnl = WordNetLemmatizer()   # Lemmatize
stop = stopwords.words('english')
w2v_file_path = 'GoogleNews-vectors-negative300.bin'  # google w2v
w2v_model = KeyedVectors.load_word2vec_format(w2v_file_path, binary=True) # w2v model
# w2v_model = Word2Vec.load(w2v_file_path)

# Data preprocessing
text = [] # All vectorized comments
for i in tqdm(range(len(data))):  # The loop preprocesses each piece of data
    docs = data.iloc[i, 0] # i: number of lines, 0: class 1 text
    words = word_tokenize(docs) # tokenize
    tagged_sent = pos_tag(words) # Gets all parts of speech in a document
    vectorization_words = [] # Each word is vectorized into a 300-dimensional [0.1,0.2,0.3] vector, which is then stored as a doc list.

    for j in range(len(words)): # Every word in every document
        if words[j].isdigit(): # Check if it's a number
            w2v = w2v_model.wv['NUM']  # num normalization
            vectorization_words.append(w2v)
        # remove stopwords
        elif words[j] in stop or not words[j].isalpha():
            continue
        else:
            wordnet_pos = get_wordnet_pos(tagged_sent[j][1]) or wordnet.NOUN
            # lemmatization
            temp = wnl.lemmatize(tagged_sent[j][0], pos=wordnet_pos)
            # case normalization
            temp = temp.lower()
            # Each word is converted into a 300-dimensional vector
            if temp in w2v_model:
                w2v = w2v_model.wv[temp]
                vectorization_words.append(w2v)
    # Comment vectorization
    # Put each vectorized comment into text, which contains all comments.
    text.append(vectorization_words)

# Handling Labels
label = data['label']
# Change classes 1 and 2 to class 3,Class 1, 2, 3, 4, 5, become class 1+2+3,4,5
for i in range(len(label)):
    if label[i] == 1 or label[i] == 2:
        label[i] = 3

# A comment becomes a vector: take the average of all words (a 300-dimensional vector)
mean_docs = []
for t in text:
    t = np.mean(t,axis = 0)
    mean_docs.append(t)

# train model
results = []
# 10-fold cross-validation
for train_index, test_index in KFold(10).split(mean_docs, label):
    text_train = []
    label_train = []
    text_test = []
    label_test = []
    # training set
    for index in train_index:
        text_train.append(mean_docs[index])
        label_train.append(label[index])
    # testing set
    for index in test_index:
        text_test.append(mean_docs[index])
        label_test.append(label[index])

    # array, easy to do matrix operations
    text_train = np.array(text_train, dtype='float32')
    label_train = np.array(label_train, dtype='float32')
    text_test = np.array(text_test)
    label_test = np.array(label_test)

    print(text_train.shape)
    print(label_train.shape)

    # Gaussian Bayesian classifier
    classifier = GaussianNB()
    # fit=train
    classifier.fit(text_train, label_train)
    # predict label
    y_pred = classifier.predict(text_test)
    # performance
    result = {}
    result['accuracy'] = metrics.accuracy_score(label_test, y_pred)
    result['f1-macro'] = f1_score(label_test, y_pred, labels=list(set(label_test)), average='macro')
    result['f1-micro'] = f1_score(label_test, y_pred, labels=list(set(label_test)), average='micro')
    result['precision-macro'] = metrics.precision_score(label_test, y_pred, average='macro')
    result['precision-micro'] = metrics.precision_score(label_test, y_pred, average='micro')
    result['recall-macro'] = metrics.recall_score(label_test, y_pred, average='macro')
    result['recall-micro'] = metrics.recall_score(label_test, y_pred, average='micro')
    results.append(result)

print('\n\nAll results：')
df = pd.DataFrame(results)
print(df)
mean_df = df.mean()
mean_df = mean_df.to_dict()
print('Cross-validation results (average of all results)：')
print(mean_df)
results.append(mean_df)
df = pd.DataFrame(results)
print("The cross-validation result is placed in the last line of the result")
print(df)
df.to_csv('bayes_cross_validation_result_3_class.csv')
print("Results save in bayes_cross_validation_result_3_class.csv")


