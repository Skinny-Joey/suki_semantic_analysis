import pandas as pd
from sklearn.model_selection import KFold
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn import metrics
from keras.callbacks import EarlyStopping
from keras.layers import Dropout
from keras.models import load_model
import gensim
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from tqdm import tqdm
from nltk import pos_tag
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer

data = pd.read_csv('fin_data_run.csv', encoding='unicode_escape')
data = data.sample(frac=1).reset_index(drop=True)


# The part of speech
def get_wordnet_pos(tag):
    if tag.startswith('J'):
        return wordnet.ADJ
    elif tag.startswith('V'):
        return wordnet.VERB
    elif tag.startswith('N'):
        return wordnet.NOUN
    elif tag.startswith('R'):
        return wordnet.ADV
    else:
        return None

wnl = WordNetLemmatizer()
# nltk.download('stopwords')
stop = stopwords.words('english')
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')

# Data preprocessing
for i in tqdm(range(len(data))):
    doc = data.iloc[i, 0]
    words = word_tokenize(doc)
    tagged_doc = pos_tag(words)
    preprocessed_words = []
    for j in range(len(words)):
        if words[j].isdigit(): #
            preprocessed_words.append('NUM')  # num normalization（文本中所有数字用NUM代替）
        # remove stopwords and punctuation
        elif words[j] in stop or not words[j].isalpha():  # remove stopwords
            continue
        else:
            wordnet_pos = get_wordnet_pos(tagged_doc[j][1]) or wordnet.NOUN
            temp = wnl.lemmatize(tagged_doc[j][0], pos=wordnet_pos)  # lemmatization
            preprocessed_words.append(temp.lower())  # case normalization
    new_doc = ''
    for w in preprocessed_words:
        new_doc += w + ' '
    data.iloc[i, 0] = new_doc
text = data['text']
label = data['label']


# LSTM modeling-------------------------------------------------------------------------------------
# Using tokenizer, each word in the text is represented by a number, which is the preprocessing of word vectorization
# texts_to_matrix: select MAX_NB_WORDS from MAX_NB_WORDS, select MAX_NB_WORDS from MAX_NB_WORDS
# We want to vectorize the cut_review data. We want to convert each cut_review to a vector of a sequence of integers
# Set the 50,000 most frequently used words
# set the maximum number of words per cut_review to 250 (more words will be cut, less words will be 0)
MAX_NB_WORDS = 50000
# The maximum length of new_doc for each preprocessed comment
MAX_SEQUENCE_LENGTH = 250
# The dimensions of each word
EMBEDDING_DIM = 100
# Make your own W2V dictionary
# Split words, convert them to numbers
tokenizer = Tokenizer(num_words=MAX_NB_WORDS, filters='!"#$%&()*+,-./:;<=>?@[\]^_`{|}~', lower=True)
tokenizer.fit_on_texts(data['text'].values)
word_index = tokenizer.word_index
print(' %s different words.' % len(word_index))
# print(tokenizer.word_index)


X = tokenizer.texts_to_sequences(data['text'].values)
# Fill X so that the length of each column of X is the same, make it 250, fill it with 0
X = pad_sequences(X, maxlen=MAX_SEQUENCE_LENGTH)
# Onehot expansion of multi-class tags
Y = pd.get_dummies(data['label']).values

print(X.shape)  # （20000，250）250 words
print(Y.shape)  #  （20000，5）5 class


# 10-fold cross-validation
# The first of the models is the Embedding layer, which uses vectors of length 100 to represent each term
# The SpatialDropout1D layer has a random ratio of 0.2 input units for each update in training, which helps prevent overfitting
# The LSTM layer contains 100 memory cells
# The output layer is a fully connected layer with 10 categories
# The activation function is set to 'softmax' because it is multi-category
# As it is multiple categorization, the loss function is categorical crossentropy _crossentropy
results = []
k = 10  # k-fold cross-validation
for train_index, test_index in tqdm(KFold(k).split(X, Y)):  # x=docs, y=label
    # model
    model = Sequential()
    model.add(Embedding(MAX_NB_WORDS, EMBEDDING_DIM, input_length=X.shape[1]))
    model.add(SpatialDropout1D(0.2))
    model.add(LSTM(100, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(3, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    print(model.summary())

    epochs = 5
    batch_size = 64
    history = model.fit(X[train_index], Y[train_index], epochs=epochs, batch_size=batch_size, validation_split=0.1,
                        callbacks=[EarlyStopping(monitor='val_loss', patience=3, min_delta=0.0001)])

    # model = load_model('lstm_class.h5')
    result = {}
    y_pred = model.predict(X[test_index])
    y_pred = y_pred.argmax(axis=1)
    y_test = Y[test_index].argmax(axis=1)
    result['accuracy'] = metrics.accuracy_score(y_test, y_pred)
    result['f1-macro'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='macro')
    result['f1-micro'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='micro')
    result['f1-weighted'] = f1_score(y_test, y_pred, labels=list(set(y_test)), average='weighted')
    result['precision-macro'] = metrics.precision_score(y_test, y_pred, average='macro')
    result['precision-micro'] = metrics.precision_score(y_test, y_pred, average='micro')
    result['precision-weighted'] = metrics.precision_score(y_test, y_pred, average='weighted')
    result['recall-macro'] = metrics.recall_score(y_test, y_pred, average='macro')
    result['recall-micro'] = metrics.recall_score(y_test, y_pred, average='micro')
    result['recall-weighted'] = metrics.recall_score(y_test, y_pred, average='weighted')
    results.append(result)

print('\n\nAll results')
df = pd.DataFrame(results)
print(df)
mean_df = df.mean()
mean_df = mean_df.to_dict()
print('Cross-validation results (average of all results)：')
print(mean_df)
results.append(mean_df)
df = pd.DataFrame(results)
print("The cross-validation result is placed in the last line of the result")
print(df)
df.to_csv('lstm_cross_validation_result_fin.csv')
print("Results saveo in lstm_cross_validation_result_fin.csv")
